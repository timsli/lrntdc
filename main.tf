# Pause resource
resource "time_sleep" "wait_container" {
  create_duration = "10s"
}

# Pull docker image for almalinux 9.3
resource "docker_image"  "almalinux93" {
  name = "almalinux:9.3"
  keep_locally = false
}

# Create container machine alamalinux9
resource "docker_container" "ctest1" {
  name = var.cntr_name
  hostname = var.cntr_hostname
  image = docker_image.almalinux93.image_id
  must_run = true
  tty = true
  stdin_open = true
  command = ["tail", "-f", "/dev/null"]
}

# Execute dnf group install Server#
resource "terraform_data"  "install_dnf_group_Server" {
  provisioner "local-exec" {
    command = "/usr/bin/docker exec -i ${docker_container.ctest1.name} sh -c \"/usr/bin/dnf group install Server --allowerasing -y\" "
  }
  depends_on = [time_sleep.wait_container] 
}

# Execute hostname of the container
resource "terraform_data" "get_container_hostname" {
  provisioner "local-exec" {
    command = "/usr/bin/docker exec -i ${docker_container.ctest1.name} sh -c hostname "
  }
  depends_on = [terraform_data.install_dnf_group_Server]
}

