# terraform settings
terraform {	
	required_providers {		
    docker = {
      source = "registry.terraform.io/kreuzwerker/docker"
      version = "3.0.2"
    }
	}
}

# Providers settings
#  We can use multiple terraform providers
provider "docker" {
  host = "unix:///var/run/docker.sock"
}

