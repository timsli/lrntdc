# Container properties variables
variable "cntr_name" {
  description = "The name of docker container"
  type = string
  default = "ctest1"
}

variable "cntr_hostname" {
  description = "The internal hostname of docker container"
  type = string
  default = "ctest1lnx1"
}

